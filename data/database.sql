
CREATE DATABASE IF NOT EXISTS baza;

CREATE TABLE `job` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL,
  `description` text NOT NULL,
  `email` varchar(256) NOT NULL,
  `status` enum('published','pending','spam') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`id`),
  KEY `ix_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=armscii8;
