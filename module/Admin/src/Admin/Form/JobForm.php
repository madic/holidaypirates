<?php
/**
 * This class for generating job form
 */
namespace Admin\Form;
use Zend\Form\Form;
use Admin\Model\JobTable;

class JobForm extends Form
{    
    protected $sm;
 
    public function __construct($sm) {
        $this->sm = $sm;
        parent::__construct('job');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/admin/job/create');
        
        $this->add(array(
            'name'       => 'title',
            'attributes' => array('type' => 'text'),
            'options'    => array(
                'label' => 'Title',
                'label_attributes' => array(
                    'class'  => 'col-xs-12 label-holder'
                )
            ),
        ));
		
        $this->add(array(
            'name'       => 'description',
            'attributes' => array('type' => 'textarea'),
            'options'    => array(
                'label' => 'Description',
                'label_attributes' => array(
                    'class'  => 'col-xs-12 label-holder'
                )
            ),
        ));
        
        $this->add(array(
            'name'       => 'email',
            'attributes' => array('type' => 'email'),
            'options'    => array(
                'label' => 'E-mail',
                'label_attributes' => array(
                    'class'  => 'col-xs-12 label-holder'
                )
            ),
        ));	
		
        $this->add(array(
            'name'       => 'submit',
            'attributes' => array(
                'type'   => 'submit',
                'value'  => 'Submit',
                'id'     => 'submitbutton',
            ),
        )); 
    }
    
    public function isValid() {
        $result = parent::isValid();
        $data = $this->getData();
        
        if ($result) {
            $jobTable = $this->sm->get('Admin\Model\JobTable');
            
            //validation was added to prevent from having jobs being published even if there is 
            //still first job pending
            if ($jobTable->userExists($data['email'], JobTable::STATUS_PENDING)) {
                $this->get('email')
                        ->setMessages(array('We found message that is pending for approval. After '
                            . 'moderator approve it you will be able to continue submitting jobs.' ));
                $result = false;
            }
        }
        return $result;
    }

}