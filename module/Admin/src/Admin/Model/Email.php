<?php

namespace Admin\Model;


use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Mime;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;

class Email {
    
    protected $emailFrom;
    protected $password;
    protected $host;
    protected $ssl;
    protected $smtpOptions;
    protected $transport;
    protected $config;

    public function __construct($sm) {
        $this->config = $config = $sm->get('Config');
        $emailParams = $config['email'];
        $this->emailFrom = $emailParams['username'];
        $this->password = $emailParams['password'];
        $this->host = $emailParams['host'];
        $this->ssl = $emailParams['ssl'];
        $this->setSmtpOptions();
        $this->setSmtpTransport();
    }
    /**
     * Sets smtp transport protocol
     */
    public function setSmtpTransport() {
        $transport = new SmtpTransport();
        $options   = new SmtpOptions($this->smtpOptions);
        $transport->setOptions($options);
        $this->transport = $transport;
    }
    /**
     * Sets smtp options 
     */
    public function setSmtpOptions() {
        $smtpOptions = new SmtpOptions();
        $smtpOptions->setHost($this->host)
                ->setConnectionClass('login')
                ->setName('mail')
                ->setConnectionConfig(array(
                    'username' => $this->emailFrom,
                    'password' => $this->password,
                    'ssl' => $this->ssl,
        ));
        $this->smtpOptions = $smtpOptions;
    }
    /**
     * Method will send confirmation email for job post
     * @param string $email
     */
    public function sendConfirmationEmail($email) {
        $message = new Message();
        $message->setEncoding('utf-8')
                ->addTo($email)
                ->addFrom($this->emailFrom)
                ->setSubject('Confirmation email')
                ->setBody("Your submition is in moderation.");
        $this->transport->send($message);
    }
    /**
     * Method will send new job post to moderator
     * @param array $data
     */
    public function sendModeratorEmail($data) {
        $view = new \Zend\View\Renderer\PhpRenderer();
        $resolver = new \Zend\View\Resolver\TemplateMapResolver();
        $resolver->setMap(array(
            'mailTemplate' => __DIR__ . '/../../../view/mails/moderator.phtml'
        ));        
        $view->setResolver($resolver);
        
        $viewModel = new \Zend\View\Model\ViewModel();
        $viewModel->setTemplate('mailTemplate')->setVariables(array(
            'title' => $data['title'],
            'description' => $data['description'],
            'spamUrl'     => $data['spamUrl'],
            'publishUrl'  => $data['publishUrl'],
        ))->setTerminal(true);
        
        $bodyPart = new \Zend\Mime\Message();
        $bodyMessage = new \Zend\Mime\Part($view->render($viewModel));
        $bodyMessage->type = 'text/html';
        $bodyPart->setParts(array($bodyMessage));

        $message = new Message();
        $message->setEncoding('utf-8')
                ->addTo($this->config['email']['moderatorEmail'])
                ->addFrom($this->emailFrom)
                ->setSubject('New job posted')
                ->setBody($bodyPart);
        $this->transport->send($message);
    }
}

