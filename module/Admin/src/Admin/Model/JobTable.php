<?php

namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;

class JobTable
{
    protected $_tableGateway;
    const STATUS_PUBLISHED = "published";
    const STATUS_PENDING = 'pending';
    const STATUS_SPAM = 'spam';
    
    public function __construct(TableGateway $tableGateway) {
        $this->_tableGateway = $tableGateway;
    }
    /**
     * Method will return job row set by id
     * @param int $id
     * @return RowSet
     * @throws \Exception
     */
    public function findByID($id) {
        $rowset = $this->_tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find job!");
        }
        return $row;
    }
    /**
     * Method will check if job with email exists
     * @param string $email
     * @param string $status
     * @return int
     */
    public function userExists($email, $status = null) {   
        $where = new Where();
        $where->equalTo('email',  $email);
        if (!is_null($status)) {
            $where->equalTo('status', $status);
        }
        $rowset = $this->_tableGateway->select($where);
        return $rowset->count();
    }
    /**
     * Method will create new job
     * @param array $post
     * @return int
     */
    public function createJob($post) {
        $approvedUser = $this->userExists($post['email']);
        $status = $approvedUser ? self::STATUS_PUBLISHED : self::STATUS_PENDING;
        $data = array(
            'email' => $post['email'],
            'title'  => $post['title'],
            'description'  => $post['description'],
            'status' => $status,
        );
        $this->_tableGateway->insert($data);
        return $this->_tableGateway->getLastInsertValue();
    }
    /**
     * Method will update job
     * @param int $id
     * @param string $status
     * @throws \Exception
     */    
    public function updateJob($id, $status) {
        if (!$this->findByID($id)) {
            throw new \Exception('Job id does not exist');
        }
        $this->_tableGateway->update(array('status' => $status), array('id' => $id));        
    }
    

}