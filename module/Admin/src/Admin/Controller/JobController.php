<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Admin\Form\JobForm;
use Admin\Model\JobTable;
use Admin\Form\JobFilter;
use Admin\Model\Email;

class JobController extends AbstractActionController {
  
    private $_jobTable;
    
    /**
     * Method will create new job
     * @return ViewModel
     */
    public function createAction() {        
        $request = $this->getRequest();
        $serviceManager = $this->getServiceLocator();
        $form    = new JobForm($serviceManager);
        if ($request->isPost()) {    
            
            $form->setInputFilter(new JobFilter());
            
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                $userExist = $this->getJobTable()->userExists($data['email']);
                $jobId = $this->getJobTable()->createJob($data);                
                if (!$userExist) {
                   $email = new Email($this->getServiceLocator());
                   $email->sendConfirmationEmail($data['email']);
                   
                   $publishUrl = $this->url()->fromRoute('admin/default', 
                           array('controller' => 'job', 'action' => 'publish-job', 'id' => $jobId), 
                           array('force_canonical' => true));
                   $spamUrl    = $this->url()->fromRoute('admin/default', 
                           array('controller' => 'job', 'action' => 'mark-as-spam', 'id' => $jobId), 
                           array('force_canonical' => true));
                   $data['publishUrl'] = $publishUrl;
                   $data['spamUrl']    = $spamUrl;                   
                   $email->sendModeratorEmail($data);
                }
                return $this->redirect()
                            ->toRoute('admin/default', 
                                array('controller' => 'job', 'action' => 'create'));
            }
        }
        return new ViewModel(array('form' => $form));
    }
    /**
     * Method will mark job as spam
     * @return ViewModel
     */
    public function markAsSpamAction() {
        $id = $this->params()->fromRoute('id');
        $this->getJobTable()->updateJob($id, JobTable::STATUS_SPAM);     
        $job = $this->getJobTable()->findByID($id);
        return new ViewModel(array('job' => $job));
    }
    /**
     * Method will publish new job
     * @return ViewModel
     */
    public function publishJobAction() {
        $id = $this->params()->fromRoute('id');
        $this->getJobTable()->updateJob($id, JobTable::STATUS_PUBLISHED);   
         $job = $this->getJobTable()->findByID($id);
        return new ViewModel(array('job' => $job));
        
    }
    /**
     * Model will return JobTable table gateway
     * @return Admin\Model\JobTable
     */
    public function getJobTable()
    {
        if (!$this->_jobTable) {
            $sm = $this->getServiceLocator();
            $this->_jobTable = $sm->get('Admin\Model\JobTable');
        }
        return $this->_jobTable;
    }
}

